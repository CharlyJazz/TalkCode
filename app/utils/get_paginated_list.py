from flask import jsonify, make_response, abort

def get_paginated_list(model, schema, url, start, limit):
  """
  start    - It is the position from which we want the data to be returned.
  schema   - It is the marsmallow schema to serialize models data
  limit    - It is the max number of items to return from that position.
  next     - It is the url for the next page of the query assuming current value of limit
  previous - It is the url for the previous page of the query assuming current value of limit
  count    - It is the total count of results available in the dataset. Here as the count is 128,
             that means you can go maximum till start=121 keeping limit as 20.
             Also when you get the page with start=121 and limit=20, 8 items will be returned.
  results  - This is the list of results whose position lies within the bounds specified by the request.
  """
  # check if page exists
  results = model.query.all()
  count = len(results)
  if (count < start):
    return False
  # make response
  obj = {}
  obj['start'] = start
  obj['limit'] = limit
  obj['count'] = count
  # make URLs
  # make previous url
  if start == 1:
    obj['has_previous'] = False
  else:
    start_copy = max(1, start - limit)
    limit_copy = start - 1
    obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)
    obj['has_previous'] = True
  # make next url
  if start + limit > count:
    obj['has_next'] = False
  else:
    start_copy = start + limit
    obj['has_next'] = True
    obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)
  # finally extract result according to bounds
  results_extracted = results[(start - 1):(start - 1 + limit)]
  model_schema = schema(many=True)
  obj['results'] = model_schema.dump(results_extracted).data

  return True, obj