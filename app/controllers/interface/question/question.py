from flask import Blueprint, request, jsonify, current_app

from ....models.questions import Question
from ....schemas.question import QuestionSchema

from ....utils.get_paginated_list import get_paginated_list

from descriptions import GET_QUESTION_DESCRIPTIONS, GET_QUESTIONS_DESCRIPTIONS

app = Blueprint('question', __name__)

@app.route('/api/questions')
def get_questions():
  pagination = get_paginated_list(
    model=Question,
    schema=QuestionSchema,
    url='/api/questions', 
    start=int(request.args.get('start', 1)), 
    limit=int(request.args.get('limit', 10))
  )

  if (pagination[0]):
    return jsonify({
      'status': 200,
      'description': GET_QUESTIONS_DESCRIPTIONS['SUCCESS'],
      'payload': pagination[1]
    }), 200

  else:
    return jsonify({
      'status' : 404,
      'description': GET_QUESTIONS_DESCRIPTIONS['NOT_FOUND']
    }), 404


@app.route('/api/questions/<int:id>')
def get_question(id):
  query = Question.query.filter_by(id=id).one_or_none()
  if (query is not None):
    schema = QuestionSchema()
    question_serialized = schema.dump(query)
    
    return jsonify({
      'status': 200,
      'description': GET_QUESTION_DESCRIPTIONS['SUCCESS'],
      'payload': question_serialized.data
    }), 200

  else:
    return jsonify({
    'status' : 404,
    'description': GET_QUESTION_DESCRIPTIONS['NOT_FOUND']
  }), 404