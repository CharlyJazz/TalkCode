import pytest, jwt

from flask import json

from ..factories import UserFactory
from ...app.models import Question
from ...app.controllers.interface.question.descriptions import GET_QUESTION_DESCRIPTIONS

@pytest.mark.usefixtures('db', 'user', 'app')
class TestGetQuestionsPagination:
  """ Test get questions with o without pagination """

  _ = 'api/questions'

  def test_get_questions_successfully(self, db, user, app):
    user = UserFactory()
    db.session.commit()

    questions_length = 30
    start = 1
    limit = 10

    for i in range(30):
      db.session.add(
        Question(id_user=user.id, text="What is the life? {}".format(i))
      )
      db.session.commit()
      
    assert Question.query.count() == questions_length # Check if the questions are already created

    r = app.test_client().get(self._ + '?start={}&limit={}'.format(start, limit))

    data = json.loads(r.data)

    assert data['payload']['start'] == start
    assert data['payload']['limit'] == limit
    assert data['payload']['count'] == questions_length
    assert data['payload']['has_previous'] == False
    assert 'previous' not in data['payload']
    assert data['payload']['has_next'] == True
    assert 'next' in data['payload']
    assert data['payload']['results'][start - 1]['text'] == "What is the life? {}".format(start - 1)
    assert data['payload']['results'][limit - 1]['text'] == "What is the life? {}".format(limit - 1)

    # Use the next url value to search more questions

    next_url = data['payload']['next'] # u'/api/questions?start=11&limit=10'
    start = 11 #TODO: Extract to next_url
    limit = 10 #TODO: Extract to next_url

    r = app.test_client().get(next_url)

    data = json.loads(r.data)

    assert data['payload']['start'] == start
    assert data['payload']['limit'] == limit
    assert data['payload']['count'] == questions_length
    assert data['payload']['has_previous'] == True
    assert 'previous' in data['payload']
    assert data['payload']['has_next'] == True
    assert 'next' in data['payload']
    assert data['payload']['results'][0]['text'] == "What is the life? {}".format(start - 1)
    assert data['payload']['results'][limit - 1]['text'] == "What is the life? {}".format(len(data['payload']['results']) + limit - 1)

    # Use the next url value to search more questions

    next_url = data['payload']['next'] # u'/api/questions?start=11&limit=10'
    start = 21 #TODO: Extract to next_url
    limit = 10 #TODO: Extract to next_url

    r = app.test_client().get(next_url)

    data = json.loads(r.data)

    assert data['payload']['start'] == start
    assert data['payload']['limit'] == limit
    assert data['payload']['count'] == questions_length
    assert data['payload']['has_previous'] == True
    assert 'previous' in data['payload']
    assert data['payload']['has_next'] == False
    assert 'next' not in data['payload']
    assert data['payload']['results'][0]['text'] == "What is the life? {}".format(start - 1)
    assert data['payload']['results'][limit - 1]['text'] == "What is the life? {}".format(questions_length - 1)


@pytest.mark.usefixtures('db', 'user', 'app')
class TestGetQuestion:
  """ Test GetQuestion route"""

  _ = 'api/questions/'

  def test_get_question_successfully(self, db, user, app):
    user = UserFactory()
    db.session.commit()
    question = Question(id_user=user.id, text="What is the life?")
    db.session.add(question)
    db.session.commit()
    r = app.test_client().get(self._ + str(question.id))

    assert r.status_code == 200
    assert 'payload' in r.data
    assert 'description' in r.data
    assert 'status' in r.data
    assert json.loads(r.data)['description'] == GET_QUESTION_DESCRIPTIONS['SUCCESS']
    assert json.loads(r.data)['status'] == 200
    assert json.loads(r.data)['payload']['id'] == question.id
    assert json.loads(r.data)['payload']['text'] == question.text
    assert json.loads(r.data)['payload']['user']['id'] == user.id
    assert json.loads(r.data)['payload']['user']['username'] == user.username
    assert json.loads(r.data)['payload']['user']['email'] == user.email
  
  def test_get_user_not_found(self, db, user, app):
    r = app.test_client().get(self._ + '1')
    
    assert r.status_code == 404
    assert 'payload' not in r.data
    assert 'description' in r.data
    assert 'status' in r.data
    assert json.loads(r.data)['description'] == GET_QUESTION_DESCRIPTIONS['NOT_FOUND']
    assert json.loads(r.data)['status'] == 404